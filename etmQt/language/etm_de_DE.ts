<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="de" sourcelanguage="">
<context>
    <name>@default</name>
    <message>
        <location filename="etmData.py" line="860"/>
        <source>today</source>
        <translation type="unfinished">heute</translation>
    </message>
    <message>
        <location filename="etmData.py" line="2992"/>
        <source>day</source>
        <translation type="unfinished">Tag</translation>
    </message>
    <message>
        <location filename="etmData.py" line="2994"/>
        <source>days</source>
        <translation type="unfinished">Tage</translation>
    </message>
    <message>
        <location filename="etmData.py" line="2998"/>
        <source>hour</source>
        <translation type="unfinished">Stunde</translation>
    </message>
    <message>
        <location filename="etmData.py" line="3000"/>
        <source>hours</source>
        <translation type="unfinished">Stunden</translation>
    </message>
    <message>
        <location filename="etmData.py" line="3004"/>
        <source>minute</source>
        <translation type="unfinished">Minute</translation>
    </message>
    <message>
        <location filename="etmData.py" line="3006"/>
        <source>minutes</source>
        <translation type="unfinished">Minuten</translation>
    </message>
    <message>
        <location filename="etmData.py" line="3013"/>
        <source>now</source>
        <translation type="unfinished">jetzt</translation>
    </message>
    <message>
        <location filename="etmData.py" line="3013"/>
        <source>%1 from now</source>
        <translation type="obsolete">%1 von jetzt</translation>
    </message>
    <message>
        <location filename="etmData.py" line="3100"/>
        <source>none</source>
        <translation type="unfinished">nichts</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="131"/>
        <source>current conditions</source>
        <translation type="obsolete">derzeitige Bedingungen</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="138"/>
        <source>wind</source>
        <translation type="obsolete">Wind</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="147"/>
        <source>feels like</source>
        <translation type="obsolete">fühlt sich an wie</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="152"/>
        <source>speed</source>
        <translation type="obsolete">Geschwindigkeit</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="157"/>
        <source>calm</source>
        <translation type="obsolete">Ruhe</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="164"/>
        <source>pressure</source>
        <translation type="obsolete">Druck</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="169"/>
        <source>visibility</source>
        <translation type="obsolete">Sichtbarkeit</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="175"/>
        <source>rising</source>
        <translation type="obsolete">steigend</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="177"/>
        <source>falling</source>
        <translation type="obsolete">fallend</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="179"/>
        <source>constant</source>
        <translation type="obsolete">konstant</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="181"/>
        <source>humidity</source>
        <translation type="obsolete">Feuchtigkeit</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="188"/>
        <source>sunrise</source>
        <translation type="obsolete">Sonnenaufgang</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="191"/>
        <source>sunset</source>
        <translation type="obsolete">Sonnenuntergang</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="195"/>
        <source>forecast</source>
        <translation type="obsolete">Vorschau</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="220"/>
        <source>high</source>
        <translation type="obsolete">hoch</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="229"/>
        <source>low</source>
        <translation type="obsolete">niedrig</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1578"/>
        <source>%s is already open</source>
        <translation type="unfinished">% ist bereits geöffnet</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1589"/>
        <source>Another file operation is in process and must be completed before this can be done.</source>
        <translation type="unfinished">Eine andere Dateioperation läuft und muss beendet sein, bevor dies geschehen kann.</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1591"/>
        <source>The editor must be closed before this can be done.</source>
        <translation type="unfinished">Der Editor muss geschlossen sein, bevor das geschehen kann.</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1594"/>
        <source>A file operation is in process and must be completed before the editor can be opened.</source>
        <translation type="unfinished">Eine Dateioperation läuft und muss beendet sein, bevor der Editor geöffnet werden kann.</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1596"/>
        <source>Only once instance of the editor can be open at one time.</source>
        <translation type="unfinished">Nur eine Instanz des Editors kann gleichzeitig geöffnet sein. </translation>
    </message>
    <message>
        <location filename="etmData.py" line="2052"/>
        <source>Ignoring invalid groupby part: &quot;{0}&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmData.py" line="2437"/>
        <source>invalid groupby setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmData.py" line="3015"/>
        <source>{0} from now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmData.py" line="3978"/>
        <source>No output was generated.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailForm</name>
    <message>
        <location filename="etmView.py" line="913"/>
        <source>Click here or press Ctrl-T to start the timer
for an action based on this item.</source>
        <translation>Klicke hier oder drücke Strg-T um die Stoppuhr
für eine Aktion zu diesem Eintrag zu starten.</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1008"/>
        <source>Click here or press Ctrl-D to delete one or more repetitions of this item.</source>
        <translation>Klicke hier oder drücke Strg-D um eine oder mehrere Wiederholungen dieses Eintrags zu löschen</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1023"/>
        <source>Click here or press Ctrl-F to set the completion datetime for this job.</source>
        <translation>Klicke hier oder drücke Strg-F um die Endzeit für diese Aufgabe zu setzen.</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1014"/>
        <source>Click here or press Ctrl-F when a task is selected
to update the latest completion datetime for the task.</source>
        <translation>Klicke hier oder drücke Strg-F, wenn eine Aufgabe ausgewählt ist,
um die späteste Endzeit für die Aufgabe zu ändern. </translation>
    </message>
    <message>
        <location filename="etmView.py" line="1019"/>
        <source>Click here or press Ctrl-D to delete this action</source>
        <translation>Klicke hier oder drücke Strg-D um diese Aktion zu löschen.</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1021"/>
        <source>Click here or press Ctrl-D to delete this item.</source>
        <translation>Klicke hier oder drücke Strg-D um diesen Eintrag zu löschen.</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1025"/>
        <source>Click here or press Ctrl-F when a task is selected
to set the finish datetime for this item</source>
        <translation>Klicke hier oder drücke Strg-F wenn eine Aufgabe ausgewählt ist,
um die Endzeit dafür zu setzen.</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1082"/>
        <source>change history</source>
        <translation>Befehls-History ändern</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1093"/>
        <source>The GNU Unified Diff Format</source>
        <translation>Das GNU Unified Diff Format</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1094"/>
        <source>
The format starts with a two-line header with the original file preceded by &quot;---&quot; and the new file is preceded by &quot;+++&quot;. Following this are one or more change hunks that contain the line differences in the file. The unchanged, contextual lines are preceded by a space character, addition lines are preceded by a plus sign, and deletion lines are preceded by a minus sign.

A hunk begins with range information and is immediately followed with the line additions, line deletions, and any number of the contextual lines. The range information is surrounded by double-at signs. The format of the range information line is as follows:

@@ -l,s +l,s @@

The hunk range information contains two hunk ranges. The range for the hunk of the original file is preceded by a minus symbol, and the range for the new file is preceded by a plus symbol. Each hunk range is of the format l,s where l is the starting line number and s is the number of lines the change hunk applies to for each respective file.

If a line is modified, it is represented as a deletion and addition. Since the hunks of the original and new file appear in the same hunk, such changes would appear adjacent to one another. An example of this is:

-check this dokument. On
+check this document. On
</source>
        <translation>Das Format beginnt mit einem zweizeiligen Kopf, wobei der Originaldatei &quot;---&quot;  vorangestellt ist, der neuen Datei hingegen &quot;+++&quot;. Darauf folgen ein oder mehrere Blöcke, die die Unterschiede in den Zeilen enthalten. Die unveränderten Zeilen in ihrem Zusammenhang starten mit einem Leerzeichen, hinzugefügte Zeilen beginnen mit einem Plus-Zeichen und gelöschte Zeilen mit einem Minus-Zeichen.

Ein Textblock beginnt mit einer Information über die Reichweite und wird gefolgt von den Hinzufügungen und  Löschungen von Zeilen sowie einer gewissen Anzahl von Zeilen aus dem Zusammenhang. Die Information über den Bereich ist durch doppelte Klammeraffen (&quot;@@&quot;) eingerahmt. Das Format einer Bereichsangabe sieht also folgendermaßen aus:

@@ -l,s +l,s @@

Die Information über die Blockweite enthält zwei Blockweiten. Der Bereich des Originalblocks beginnt mit einem Minus-Zeichen, der für den neuen Block mit einem Plus-Zeichen. Jede Blockweite hat das Format &quot;l,s&quot;, wobei &quot;l&quot; die Startzeile und &quot;s&quot; die Anzahl der Zeilen für den Veränderungsblock ist, der auf jede Datei angewandt wird. 

Wenn eine Zeile verändert ist, wird dies als Löschung und Einfügung dargestellt. Da die Blöcke des Originals und der neuen Datei im selben Block erscheinen, erscheinen solche Änderungen nebeneinander. Ein Beispiel dazu:

-überprüfe dieses Dockument
+überprüfe dieses Dokument</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1132"/>
        <source>repetitions</source>
        <translation>Wiederholungen</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1132"/>
        <source>times are</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmView.py" line="1140"/>
        <source>repetitions for {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmView.py" line="1295"/>
        <source>datetime</source>
        <translation>Tag und Uhrzeit</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1240"/>
        <source>Adding item to {1} failed - aborted removing item from {2}: {3}</source>
        <translation>Hinzufügen von Ereignis {1} fehlgeschlagen - Abbruch des Entfernens von {2}: {3}</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1295"/>
        <source>new date and time to replace
%s</source>
        <translation>zu verändertes neues Datum und neue Zeit 
%s</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1462"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1463"/>
        <source>Do you really want to delete this item?

This action cannot be undone.</source>
        <translation>Willst Du diesen Eintrag wirklich löschen?

Diese Aktion kann nicht rückgängig gemacht werden.</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1497"/>
        <source>copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1527"/>
        <source>action</source>
        <translation>Aktion</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1546"/>
        <source>active timer</source>
        <translation>aktive Stoppuhr</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="ui_etmBrowser.py" line="42"/>
        <source>etm</source>
        <translation>etm</translation>
    </message>
    <message>
        <location filename="ui_etmList.py" line="65"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="ui_etmCalendars.py" line="44"/>
        <source>Calendars</source>
        <translation>Kalender</translation>
    </message>
    <message>
        <location filename="ui_etmCalendars.py" line="45"/>
        <source>Click here or press Ctrl-E to export the selected calendars.</source>
        <translation>Klicke hier oder drücke Strg-E um die ausgewählten Kalender zu exportieren</translation>
    </message>
    <message>
        <location filename="ui_etmCalendars.py" line="46"/>
        <source>Limit display to selected calendars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmHelp.py" line="141"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="ui_etmHelp.py" line="142"/>
        <source>Overview</source>
        <translation>Überblick</translation>
    </message>
    <message>
        <location filename="ui_etmHelp.py" line="143"/>
        <source>Data</source>
        <translation>Daten</translation>
    </message>
    <message>
        <location filename="ui_etmHelp.py" line="144"/>
        <source>Views</source>
        <translation>Ansichten</translation>
    </message>
    <message>
        <location filename="ui_etmHelp.py" line="145"/>
        <source>Reports</source>
        <translation>Reporte</translation>
    </message>
    <message>
        <location filename="ui_etmHelp.py" line="146"/>
        <source>Shortcuts</source>
        <translation>Tastenkürzel</translation>
    </message>
    <message>
        <location filename="ui_etmHelp.py" line="147"/>
        <source>Preferences</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="ui_etmHelp.py" line="148"/>
        <source>Click here or press Shift-Ctrl-G to search backwards.</source>
        <translation>Klicke hier oder drücke Strg-G um rückwärts zu suchen</translation>
    </message>
    <message>
        <location filename="ui_etmList.py" line="67"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="ui_etmHelp.py" line="150"/>
        <source>Click here or press Ctrl-F to enter a search expression.</source>
        <translation>Klicke hier oder drücke Strg-F um einen zu suchenden Ausdruck einzugeben</translation>
    </message>
    <message>
        <location filename="ui_etmHelp.py" line="151"/>
        <source>search</source>
        <translation>suchen</translation>
    </message>
    <message>
        <location filename="ui_etmHelp.py" line="152"/>
        <source>Click here or press Ctrl-G to search forwards.</source>
        <translation>Klicke hier oder drücke Strg-G um vorwärts zu suchen</translation>
    </message>
    <message>
        <location filename="ui_etmList.py" line="68"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="ui_etmList.py" line="66"/>
        <source>TextLabel</source>
        <translation>TextMarkierung</translation>
    </message>
</context>
<context>
    <name>EditForm</name>
    <message>
        <location filename="etmView.py" line="1668"/>
        <source>&apos;auto_completions&apos; not found or empty</source>
        <translation>automatische Vervollständigungen nicht gefunden oder leer</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1820"/>
        <source>backward</source>
        <translation>rückwärts</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1823"/>
        <source>forward</source>
        <translation>vorwärts</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1834"/>
        <source>not found</source>
        <translation>nicht gefunden</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1860"/>
        <source>The document has been modified.</source>
        <translation>Das Dokument wurde verändert</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1861"/>
        <source>Do you want to verify and save your changes?</source>
        <translation>Willst Du Deine Veränderungen speichern?</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1952"/>
        <source>changes saved</source>
        <translation>Veränderungen gespeichert</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1961"/>
        <source>replaced lines</source>
        <translation>ersetzte Zeilen</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1976"/>
        <source>added lines</source>
        <translation>hinzugefügte Zeilen</translation>
    </message>
    <message>
        <location filename="etmView.py" line="1982"/>
        <source>removed lines</source>
        <translation>entfernte Zeilen</translation>
    </message>
    <message>
        <location filename="etmView.py" line="2053"/>
        <source>Please verify</source>
        <translation>bitte überprüfen</translation>
    </message>
    <message>
        <location filename="etmView.py" line="2081"/>
        <source>edited file</source>
        <translation>editierte Datei</translation>
    </message>
    <message>
        <location filename="etmView.py" line="2123"/>
        <source>select file</source>
        <translation>wähle Datei aus</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="ui_etmSearch.py" line="60"/>
        <source>Search</source>
        <translation>Suche</translation>
    </message>
    <message>
        <location filename="ui_etmSearch.py" line="58"/>
        <source>Click here or press Shift Ctrl-G to search backward.</source>
        <translation>Klicke hier oder drücke Strg-G um rückwärts zu suchen.</translation>
    </message>
    <message>
        <location filename="ui_etmSearch.py" line="59"/>
        <source>&amp;Previous</source>
        <translation>&amp;Voriges</translation>
    </message>
    <message>
        <location filename="ui_etmSearch.py" line="61"/>
        <source>Click here or press Ctrl-G to search forward.</source>
        <translation>Klicke hier oder drücke Strg-G um vorwärts zu suchen.</translation>
    </message>
    <message>
        <location filename="ui_etmSearch.py" line="62"/>
        <source>&amp;Next</source>
        <translation>&amp;Nächstes</translation>
    </message>
</context>
<context>
    <name>HelpForm</name>
    <message>
        <location filename="etmView.py" line="397"/>
        <source>backward</source>
        <translation>rückwärts</translation>
    </message>
    <message>
        <location filename="etmView.py" line="400"/>
        <source>forward</source>
        <translation>vorwärts</translation>
    </message>
    <message>
        <location filename="etmView.py" line="411"/>
        <source>not found</source>
        <translation>nicht gefunden</translation>
    </message>
</context>
<context>
    <name>ReportForm</name>
    <message>
        <location filename="etmView.py" line="2328"/>
        <source>CSV files (*.csv)</source>
        <translation>CSV-Dateien (*.csv)</translation>
    </message>
    <message>
        <location filename="etmView.py" line="2374"/>
        <source>The list of reports has been modified.</source>
        <translation>Die Reportliste wurde verändert.</translation>
    </message>
    <message>
        <location filename="etmView.py" line="2375"/>
        <source>Do you want to save the changes?</source>
        <translation>Willst Du die Änderungen speichern?</translation>
    </message>
    <message>
        <location filename="etmView.py" line="2455"/>
        <source>send email</source>
        <translation>verschicke Email</translation>
    </message>
    <message>
        <location filename="etmView.py" line="2455"/>
        <source>comma separated email addresses:</source>
        <translation>Komma-getrennte Email-Adresse</translation>
    </message>
    <message>
        <location filename="etmView.py" line="2462"/>
        <source>sending ...</source>
        <translation>sende ...</translation>
    </message>
    <message>
        <location filename="etmView.py" line="2472"/>
        <source>sent</source>
        <translation>gesendet</translation>
    </message>
    <message>
        <location filename="etmView.py" line="2322"/>
        <source>done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmView.py" line="2366"/>
        <source>Press Ctrl-R to
create report.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmView.py" line="2362"/>
        <source>Press return to make modification
temporarily available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmView.py" line="2493"/>
        <source>backward</source>
        <translation type="unfinished">rückwärts</translation>
    </message>
    <message>
        <location filename="etmView.py" line="2496"/>
        <source>forward</source>
        <translation type="unfinished">vorwärts</translation>
    </message>
    <message>
        <location filename="etmView.py" line="2507"/>
        <source>not found</source>
        <translation type="unfinished">nicht gefunden</translation>
    </message>
</context>
<context>
    <name>UiWindow</name>
    <message>
        <location filename="etmView.py" line="2855"/>
        <source>Day</source>
        <translation>Tag</translation>
    </message>
    <message>
        <location filename="etmView.py" line="3564"/>
        <source>Week</source>
        <translation>Woche</translation>
    </message>
    <message>
        <location filename="etmView.py" line="2855"/>
        <source>Month</source>
        <translation>Monat</translation>
    </message>
    <message>
        <location filename="etmView.py" line="2855"/>
        <source>Now</source>
        <translation>Jetzt</translation>
    </message>
    <message>
        <location filename="etmView.py" line="2855"/>
        <source>Next</source>
        <translation>Nächstes</translation>
    </message>
    <message>
        <location filename="etmView.py" line="2855"/>
        <source>Folder</source>
        <translation>Ordner</translation>
    </message>
    <message>
        <location filename="etmView.py" line="2855"/>
        <source>Keyword</source>
        <translation>Schlüsselwort</translation>
    </message>
    <message>
        <location filename="etmView.py" line="2855"/>
        <source>Tag</source>
        <translation>Markierung</translation>
    </message>
    <message>
        <location filename="etmView.py" line="3255"/>
        <source>No error messages were generated when data files were last loaded.</source>
        <translation>Es wurden keine Fehlermeldungen erzeugt, als die Dateien letztmalig geladen wurden. </translation>
    </message>
    <message>
        <location filename="etmView.py" line="3258"/>
        <source>data errors</source>
        <translation>Daten-Fehler</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4103"/>
        <source>Click here or press &lt;em&gt;Ctrl-A&lt;/em&gt; to see the remaining alerts for today.</source>
        <translation>Klicke hier oder drücke Strg-A, um die für heute verbleibenden Warnungen anzuzeigen.</translation>
    </message>
    <message>
        <location filename="etmView.py" line="3453"/>
        <source>using default calendars</source>
        <translation>benutze Standard-Kalender</translation>
    </message>
    <message>
        <location filename="etmView.py" line="3456"/>
        <source>not using default calendars</source>
        <translation>benutze nicht den Standard-Kalender</translation>
    </message>
    <message>
        <location filename="etmView.py" line="3702"/>
        <source>new event</source>
        <translation>neues Ereignis</translation>
    </message>
    <message>
        <location filename="etmView.py" line="3714"/>
        <source>expand to depth</source>
        <translation>expandiere bis zur Tiefe</translation>
    </message>
    <message>
        <location filename="etmView.py" line="3714"/>
        <source>depth (0 expands all)</source>
        <translation>Tiefe (0 expandiert alles)</translation>
    </message>
    <message>
        <location filename="etmView.py" line="3787"/>
        <source>changes saved</source>
        <translation>Veränderungen gespeichert</translation>
    </message>
    <message>
        <location filename="etmView.py" line="3998"/>
        <source>A sound alert failed. The setting for &apos;alert_soundcmd&apos; is missing from your etm.cfg.</source>
        <translation>Eine klangliche Warnung misslang. Die Einstellung für &apos;alert_soundcmd&apos; fehlt in Deiner etm.cfg-Datei.</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4005"/>
        <source>A display alert failed. The setting for &apos;alert_displaycmd&apos; is missing from your etm.cfg.</source>
        <translation>Eine Warnung durch Anzeige misslang. Die Einstellung für &apos;alert_displaycmd&apos; fehlt in Deiner etm.cfg-Datei.</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4012"/>
        <source>An email alert failed. The setting for &apos;alert_voicecmd&apos; is missing from your etm.cfg.</source>
        <translation>Eine Email-Warnung misslang. Die Einstellung für &apos;alert_voicecmd&apos; fehlt in Deiner etm.cfg-Datei.</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4026"/>
        <source>An email alert failed. Settings for the following variables are missing from your etm.cfg: %s., </source>
        <translation>Eine Email-Warnung misslang. Die Einstellungen für die folgenden Variablen fehlen in Deiner etm.cfg-Datei: %s.,</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4072"/>
        <source>A text alert failed. Settings for the following variables are missing from your &apos;emt.cfg&apos;: %s., </source>
        <translation>Eine Text-Warnung misslang. Die Einstellungen für die folgenden Variablen fehlen in Deiner etm.cfg-Datei: %s.,</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4139"/>
        <source>jump to date</source>
        <translation>springe zu Datum</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4139"/>
        <source>date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4157"/>
        <source>scratch pad</source>
        <translation></translation>
    </message>
    <message>
        <location filename="etmView.py" line="4165"/>
        <source>datetime calculator</source>
        <translation>Datums- und Uhrzeit-Rechner</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4165"/>
        <source>
Enter an expression of the form &quot;x [+-] y&quot; where x is a date
and y is either a date or a time period if &quot;-&quot; is used and
a time period if &quot;+&quot; is used. </source>
        <translation>Füge einen Ausdruck der Form &quot;x[+-]y&quot; ein, wobei x ein Datum
und y entweder ein Datum oder ein Zeitabschnitt ist, wenn &quot;-&quot; benutzt wird
und ein Zeitabschnitt, wenn &quot;+&quot; benutzt wird.</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4183"/>
        <source>etm date calculator</source>
        <translation>etm Datumsrechner</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4232"/>
        <source>action</source>
        <translation>Aktion</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4251"/>
        <source>new action</source>
        <translation>Neue Aktion</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4406"/>
        <source>summary</source>
        <translation>Zusammenfassung</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4268"/>
        <source>active timer</source>
        <translation>aktive Stoppuhr</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4295"/>
        <source>Click here or press Ctrl-T to pause the action timer.</source>
        <translation>Klicke hier oder drücke Strg-T um die Stoppuhr (zeitweilig) anzuhalten (Pause-Funktion).</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4281"/>
        <source>Click here or press Shift Ctrl-T to stop the timer and record the action.</source>
        <translation>Klicke hier oder drücke Strg-T um die Stoppuhr anzuhalten und die Aktion aufzunehmen.</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4297"/>
        <source>running</source>
        <translation>läuft</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4290"/>
        <source>paused</source>
        <translation>angehalten</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4330"/>
        <source>new item</source>
        <translation>neuer Eintrag</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4368"/>
        <source>Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmView.py" line="4406"/>
        <source>alert</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4406"/>
        <source>event</source>
        <translation>Ereignis</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4406"/>
        <source>type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4419"/>
        <source>none</source>
        <translation>nichts</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4421"/>
        <source>remaining alerts for today</source>
        <translation>verbleibende Warnungen für heute</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4431"/>
        <source>etm update information</source>
        <translation>etm Update-Information</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4439"/>
        <source>Could not load the required python package &apos;vobject&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmView.py" line="4451"/>
        <source>etm vcalendar export</source>
        <translation>etm Kalender-Export</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4473"/>
        <source>yahoo weather information</source>
        <translation>Yahoo Wetter-Infos</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4488"/>
        <source>USNO sunmoon information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmView.py" line="3480"/>
        <source>Nothing to show in day view.</source>
        <translation>Nichts anzuzeigen in der Tagesansicht. </translation>
    </message>
    <message>
        <location filename="etmView.py" line="3491"/>
        <source>Nothing for %s.</source>
        <translation>Nichts für %s.</translation>
    </message>
    <message>
        <location filename="etmView.py" line="4153"/>
        <source>scratchpad is already open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmView.py" line="4478"/>
        <source>The setting for &apos;weather_location&apos; in etm.cfg is required for this operation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmView.py" line="4493"/>
        <source>The setting for &apos;sunmoon_location&apos; in etm.cfg is required for this operation.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Weather</name>
    <message>
        <location filename="etmWeather.py" line="58"/>
        <source>N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="59"/>
        <source>NNE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="60"/>
        <source>NE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="61"/>
        <source>ENE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="62"/>
        <source>E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="63"/>
        <source>ESE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="64"/>
        <source>SE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="65"/>
        <source>SSE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="66"/>
        <source>S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="67"/>
        <source>SSW</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="68"/>
        <source>SW</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="69"/>
        <source>WSW</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="70"/>
        <source>W</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="71"/>
        <source>WNW</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="72"/>
        <source>NW</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="73"/>
        <source>NNW</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="153"/>
        <source>current conditions</source>
        <translation>derzeitige Bedingungen</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="160"/>
        <source>wind</source>
        <translation>Wind</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="169"/>
        <source>feels like</source>
        <translation>fühlt sich an wie</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="174"/>
        <source>speed</source>
        <translation>Geschwindigkeit</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="179"/>
        <source>calm</source>
        <translation>Ruhe</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="186"/>
        <source>pressure</source>
        <translation>Druck</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="191"/>
        <source>visibility</source>
        <translation>Sichtbarkeit</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="197"/>
        <source>rising</source>
        <translation>steigend</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="199"/>
        <source>falling</source>
        <translation>fallend</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="201"/>
        <source>constant</source>
        <translation>konstant</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="203"/>
        <source>humidity</source>
        <translation>Feuchtigkeit</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="210"/>
        <source>sunrise</source>
        <translation>Sonnenaufgang</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="213"/>
        <source>sunset</source>
        <translation>Sonnenuntergang</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="217"/>
        <source>forecast</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="242"/>
        <source>high</source>
        <translation>hoch</translation>
    </message>
    <message>
        <location filename="etmWeather.py" line="251"/>
        <source>low</source>
        <translation>niedrig</translation>
    </message>
</context>
<context>
    <name>WeekView</name>
    <message>
        <location filename="etmView.py" line="2748"/>
        <source>Scheduled times for</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WhichForm</name>
    <message>
        <location filename="etmView.py" line="852"/>
        <source>Append item to which file?</source>
        <translation>Eintrag an Datei ...  anhängen?</translation>
    </message>
    <message>
        <location filename="etmView.py" line="858"/>
        <source>only the datetime of this instance</source>
        <translation>nur Tag und Uhrzeit dieser Instanz</translation>
    </message>
    <message>
        <location filename="etmView.py" line="859"/>
        <source>this instance</source>
        <translation>diese Instanz</translation>
    </message>
    <message>
        <location filename="etmView.py" line="860"/>
        <source>this and all subsequent instances</source>
        <translation>diese und alle folgenden Instanzen</translation>
    </message>
    <message>
        <location filename="etmView.py" line="861"/>
        <source>all instances</source>
        <translation>alle Instanzen</translation>
    </message>
    <message>
        <location filename="etmView.py" line="863"/>
        <source>You have selected instance</source>
        <translation>Du hast Instanz ... gewählt</translation>
    </message>
    <message>
        <location filename="etmView.py" line="865"/>
        <source>of a repeating item. What do you want to delete?</source>
        <translation>eines wiederrholten Eintrags. Was willst Du löschen?</translation>
    </message>
    <message>
        <location filename="etmView.py" line="868"/>
        <source>of a repeating item. What do you want to change?</source>
        <translation>eines wiederrholten Eintrags. Was willst Du ändern?</translation>
    </message>
</context>
<context>
    <name>detailDialog</name>
    <message>
        <location filename="ui_etmDetails.py" line="177"/>
        <source>details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="ui_etmDetails.py" line="178"/>
        <source>Click here or press &lt;em&gt;Ctrl-W&lt;/em&gt; to close this dialog.</source>
        <translation>Klicke hier oder drücke Strg-W um den Dialog zu schließen</translation>
    </message>
    <message>
        <location filename="ui_etmDetails.py" line="179"/>
        <source>Close</source>
        <translation>schließen</translation>
    </message>
    <message>
        <location filename="ui_etmDetails.py" line="180"/>
        <source>Click here or press &lt;em&gt;Ctrl-E&lt;/em&gt; to edit the file containing this item.</source>
        <translation>Klicke hier oder drücke Strg-E um die Datei, die diesen Eintrag enthält, zu editieren. </translation>
    </message>
    <message>
        <location filename="ui_etmDetails.py" line="181"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="ui_etmDetails.py" line="182"/>
        <source>Click here or press &lt;em&gt;Return&lt;/em&gt; to edit this item.</source>
        <translation>Klicke hier oder drücke Enter um diesen Eintrag zu editieren. </translation>
    </message>
    <message>
        <location filename="ui_etmDetails.py" line="183"/>
        <source>Edit</source>
        <translation>Editieren</translation>
    </message>
    <message>
        <location filename="ui_etmDetails.py" line="184"/>
        <source>Click here or press &lt;em&gt;Ctrl-C&lt;/em&gt; to edit a copy of this item.</source>
        <translation>Klicke hier oder drücke Strg-C um eine Kopie dieses Eintrags zu editieren. </translation>
    </message>
    <message>
        <location filename="ui_etmDetails.py" line="199"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="ui_etmDetails.py" line="186"/>
        <source>Click here or press &lt;em&gt;Ctrl-T&lt;/em&gt; to start the timer for an action based on this item.</source>
        <translation>Klicke hier oder drücke Strg-T um die Stoppuhr für eine Aktion zu diesem Eintrag zu starten. </translation>
    </message>
    <message>
        <location filename="ui_etmDetails.py" line="188"/>
        <source>Click here or press &lt;em&gt;Ctrl-F&lt;/em&gt; when a task is selected to set the completion datetime for the task. </source>
        <translation>Klicke hier oder drücke Strg-F, wenn eine Aufgabe ausgewählt ist, um die Endzeit der Aufgabe einzutragen. </translation>
    </message>
    <message>
        <location filename="ui_etmDetails.py" line="189"/>
        <source>Finish</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="ui_etmDetails.py" line="190"/>
        <source>Click here or press &lt;em&gt;Ctrl-X&lt;/em&gt; to delete datetime(s) from this item.</source>
        <translation>Klicke hier oder drücke Strg-X um Datum und Uhrzeit(en) aus diesem Eintrag zu löschen. </translation>
    </message>
    <message>
        <location filename="ui_etmDetails.py" line="191"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="ui_etmDetails.py" line="192"/>
        <source>Click here or press &lt;em&gt;Ctrl-M&lt;/em&gt; to move this item to another file.</source>
        <translation>Klicke hier oder drücke Strg-M, um diesen Eintrag in eine andere Datei zu verschieben. </translation>
    </message>
    <message>
        <location filename="ui_etmDetails.py" line="194"/>
        <source>Click here or press &lt;em&gt;Ctrl-R&lt;/em&gt; when a repeating item is selected to show the repetitions for the item.</source>
        <translation>Klicke hier oder drücke Strg-R, wenn ein sich wiederholender Eintrag ausgewählt ist, um die Wiederholungen anzuzeigen.  </translation>
    </message>
    <message>
        <location filename="ui_etmDetails.py" line="196"/>
        <source>Click here or press &lt;em&gt;Ctrl-H&lt;/em&gt; to view the history of changes for the file containing this item.</source>
        <translation>Klicke hier oder drücke Strg-H, um die History der Veränderungen für die Datei, die diesen Eintrag enthält, anzuzeigen. </translation>
    </message>
    <message>
        <location filename="ui_etmDetails.py" line="198"/>
        <source>Click here or press &apos;F1&apos; to view help information.</source>
        <translation>Klicke hier oder drücke F1 um Hilfe zu erhalten. </translation>
    </message>
</context>
<context>
    <name>editDialog</name>
    <message>
        <location filename="ui_etmEditor.py" line="236"/>
        <source>Editor</source>
        <translation>Editor</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="237"/>
        <source>Click here or press Ctrl-W to quit, with a prompt
to verify and save changes if modified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="239"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="240"/>
        <source>Click here or press Ctrl-S  to verify and save the changes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="241"/>
        <source>Save</source>
        <translation>Sichern</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="242"/>
        <source>Click here or press Ctrl-X to delete the
selection and copy it to the clipboard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="244"/>
        <source>Cut</source>
        <translation>Ausschneiden</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="245"/>
        <source>Click here or press Ctrl-C to copy
the selection to the clipboard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="247"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="248"/>
        <source>Click here or press Ctrl-V to paste
the clipboard text at the cursor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="250"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="251"/>
        <source>Click here or press Shift-Ctrl-G to search backwards.</source>
        <translation type="unfinished">Klicke hier oder drücke Strg-G um rückwärts zu suchen</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="256"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="253"/>
        <source>Click here or press Ctrl-F to enter a search expression.</source>
        <translation type="unfinished">Klicke hier oder drücke Strg-F um einen zu suchenden Ausdruck einzugeben</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="254"/>
        <source>search</source>
        <translation>suchen</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="255"/>
        <source>Click here or press Ctrl-G to search forwards.</source>
        <translation type="unfinished">Klicke hier oder drücke Strg-G um vorwärts zu suchen</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="257"/>
        <source>Click here or press &apos;F1&apos; to view help information.</source>
        <translation type="unfinished">Klicke hier oder drücke F1 um Hilfe zu erhalten. </translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="258"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="259"/>
        <source>View</source>
        <translation>Anschauen</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="260"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="263"/>
        <source>New project</source>
        <translation>Neues Projekt</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="262"/>
        <source>Edit project</source>
        <translation>Editiere Projekt</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="264"/>
        <source>New action</source>
        <translation>Neue Aktion</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="265"/>
        <source>Finish task</source>
        <translation>Beende Aufgabe</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="266"/>
        <source>Delete item</source>
        <translation>Lösche Eintrag</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="267"/>
        <source>Now</source>
        <translation>Jetzt</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="268"/>
        <source>Next</source>
        <translation>Nächstes</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="269"/>
        <source>Day</source>
        <translation>Tag</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="270"/>
        <source>Week</source>
        <translation>Woche</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="271"/>
        <source>Month</source>
        <translation>Monat</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="272"/>
        <source>Project</source>
        <translation>Projekt</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="273"/>
        <source>Notes</source>
        <translation>Notizen</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="274"/>
        <source>Actions</source>
        <translation>Aktionen</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="278"/>
        <source>Selection history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="276"/>
        <source>Custom</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="277"/>
        <source>Selection details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="279"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="280"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="281"/>
        <source>Restart</source>
        <translation>Starte neu</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="282"/>
        <source>Record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="283"/>
        <source>Edit</source>
        <translation>Editiere</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="284"/>
        <source>Finish</source>
        <translation>beende</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="285"/>
        <source>Delete</source>
        <translation>lösche</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="286"/>
        <source>Move</source>
        <translation>verschiebe</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="287"/>
        <source>all day event</source>
        <translation>tägliches Ereignis</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="288"/>
        <source>Ctrl+L</source>
        <translation>Strg-L</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="289"/>
        <source>event</source>
        <translation>Ereignis</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="290"/>
        <source>Ctrl+E</source>
        <translation>Strg-E</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="291"/>
        <source>task</source>
        <translation type="unfinished">Aufgabe</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="292"/>
        <source>Ctrl+T</source>
        <translation>Strg-T</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="293"/>
        <source>task group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="294"/>
        <source>Ctrl+G</source>
        <translation>Strg-G</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="295"/>
        <source>delegated task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="296"/>
        <source>Ctrl+D</source>
        <translation>Strg-D</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="297"/>
        <source>in basket</source>
        <translation>im Korb</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="298"/>
        <source>Ctrl+I</source>
        <translation>Strg-I</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="299"/>
        <source>someday maybe</source>
        <translation>vielleicht eines Tages</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="300"/>
        <source>Ctrl+S</source>
        <translation>Strg-S</translation>
    </message>
    <message>
        <location filename="ui_etmEditor.py" line="301"/>
        <source>find</source>
        <translation>Suche</translation>
    </message>
</context>
<context>
    <name>reportDialog</name>
    <message>
        <location filename="ui_etmReport.py" line="174"/>
        <source>report</source>
        <translation type="unfinished">Report</translation>
    </message>
    <message>
        <location filename="ui_etmReport.py" line="175"/>
        <source>Click here or press Ctrl-W to close this view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmReport.py" line="198"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="ui_etmReport.py" line="177"/>
        <source>Click here or press Ctrl-P to print this report.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmReport.py" line="179"/>
        <source>Click here or press Ctrl-E to export this report in CSV format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmReport.py" line="189"/>
        <source>Click here or press &apos;F1&apos; to view help information.</source>
        <translation type="unfinished">Klicke hier oder drücke F1 um Hilfe zu erhalten. </translation>
    </message>
    <message>
        <location filename="ui_etmReport.py" line="193"/>
        <source>Click here or press Ctrl-S  to save the changes to the list of reports.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmReport.py" line="191"/>
        <source>Click here or press Delete to remove the selected report.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmReport.py" line="181"/>
        <source>Click here or press Ctrl-M to email this report.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmReport.py" line="183"/>
        <source>Click here or press Shift-Ctrl-G to search backwards.</source>
        <translation type="unfinished">Klicke hier oder drücke Strg-G um rückwärts zu suchen</translation>
    </message>
    <message>
        <location filename="ui_etmReport.py" line="185"/>
        <source>Click here or press Ctrl-F to enter a search expression.</source>
        <translation type="unfinished">Klicke hier oder drücke Strg-F um einen zu suchenden Ausdruck einzugeben</translation>
    </message>
    <message>
        <location filename="ui_etmReport.py" line="186"/>
        <source>search</source>
        <translation type="unfinished">suchen</translation>
    </message>
    <message>
        <location filename="ui_etmReport.py" line="187"/>
        <source>Click here or press Ctrl-G to search forwards.</source>
        <translation type="unfinished">Klicke hier oder drücke Strg-G um vorwärts zu suchen</translation>
    </message>
    <message>
        <location filename="ui_etmReport.py" line="195"/>
        <source>report specification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmReport.py" line="196"/>
        <source>Click here or press Ctrl-R to refresh the report
using the current specification.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmReport.py" line="199"/>
        <source>report specificaton</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>uiMainWindow</name>
    <message>
        <location filename="ui_etmView.py" line="421"/>
        <source>etm</source>
        <translation>etm</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="421"/>
        <source>Click here or press &lt;em&gt;Ctrl-V&lt;/em&gt; to select the view.</source>
        <translation type="obsolete">Klicke hier oder drücke Strg-V um die Sicht auszuwählen.</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="423"/>
        <source>Click here or press &lt;em&gt;Ctrl-N&lt;/em&gt;  to create a new event, note or task.</source>
        <translation>Klicke hier oder drücke Strg-N um ein neues Ereignis, eine neue Notiz oder eine neue Aufgabe zu erstellen. </translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="447"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="425"/>
        <source>Click here or press &lt;em&gt;Ctrl-T&lt;/em&gt; to start a timer for a new action.</source>
        <translation>Klicke hier oder drücke Strg-T, um die Stoppuhr für eine neue Aktion zu starten.</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="427"/>
        <source>Click here or press &lt;em&gt;Ctrl-R&lt;/em&gt; to create a report.</source>
        <translation>Klicke hier oder drücke Strg-R, um einen Report zu erstellen. </translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="428"/>
        <source>report</source>
        <translation>Report</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="429"/>
        <source>Click here or press Ctrl-F to enter a pattern to limit
the display to items from matching branches.</source>
        <translation>Klicke hier oder drücke Strg-F, ....</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="431"/>
        <source>filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="432"/>
        <source>Click here or press &lt;em&gt;F1&lt;/em&gt; to view help information.</source>
        <translation>Klicke hier oder drücke F1, um Hilfe zu erhalten.</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="433"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="434"/>
        <source>Click here or press &lt;em&gt;Ctrl-A&lt;/em&gt; to see the remaining alerts for today.</source>
        <translation>Klicke hier oder drücke Strg-A, um die restlichen Warnungen für heute zu sehen. </translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="436"/>
        <source>current time</source>
        <translation>aktuelle Zeit</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="437"/>
        <source>0:00</source>
        <translation>0:00</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="438"/>
        <source>Click here or press &lt;em&gt;Ctrl-T&lt;/em&gt; to pause the timer.</source>
        <translation>Klicke hier oder drücke Strg-T, um die Stoppuhr (zeitweise) anzuhalten (Pause).</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="440"/>
        <source>Click here or press &lt;em&gt;Shift Ctrl-T&lt;/em&gt; to stop the timer and record the action.</source>
        <translation>Klicke hier oder drücke Umschalt-Strg-T, </translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="442"/>
        <source>Click here or press the &lt;em&gt;period&lt;/em&gt; key to view past due items.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="446"/>
        <source>Click here or press &lt;em&gt;Ctrl-E&lt;/em&gt; to see data error messages.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="448"/>
        <source>View</source>
        <translation>ansehen</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="449"/>
        <source>Add</source>
        <translation>hinzufügen</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="452"/>
        <source>New project</source>
        <translation>Neues Projekt</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="451"/>
        <source>Edit project</source>
        <translation>Editiere Projekt</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="453"/>
        <source>New action</source>
        <translation>Neue Aktion</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="454"/>
        <source>Finish task</source>
        <translation>Beende Aufgabe</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="455"/>
        <source>Delete item</source>
        <translation>Lösche Eintrag</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="456"/>
        <source>Now</source>
        <translation>jetzt</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="457"/>
        <source>Next</source>
        <translation>nächstes</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="458"/>
        <source>Day</source>
        <translation>Tag</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="459"/>
        <source>Week</source>
        <translation>Woche</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="460"/>
        <source>Month</source>
        <translation>Monat</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="461"/>
        <source>Project</source>
        <translation>Projekt</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="462"/>
        <source>Notes</source>
        <translation>Notizen</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="463"/>
        <source>Actions</source>
        <translation>Aktionen</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="467"/>
        <source>Selection history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="465"/>
        <source>Custom</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="466"/>
        <source>Selection details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="468"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="469"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="470"/>
        <source>Restart</source>
        <translation>starte neu</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="471"/>
        <source>Record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="472"/>
        <source>Edit</source>
        <translation>Editiere</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="473"/>
        <source>Finish</source>
        <translation>Beende</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="474"/>
        <source>Delete</source>
        <translation>Lösche</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="475"/>
        <source>Move</source>
        <translation>Verschiebe</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="476"/>
        <source>all day event</source>
        <translation>tägliches Ereignis</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="477"/>
        <source>Ctrl+L</source>
        <translation>Strg-L</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="478"/>
        <source>event</source>
        <translation>Ereignis</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="479"/>
        <source>Ctrl+E</source>
        <translation>Strg-E</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="480"/>
        <source>task</source>
        <translation>Aufgabe</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="481"/>
        <source>Ctrl+T</source>
        <translation>Strg-T</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="482"/>
        <source>task group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="483"/>
        <source>Ctrl+G</source>
        <translation>Strg-G</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="484"/>
        <source>delegated task</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="485"/>
        <source>Ctrl+D</source>
        <translation>Strg-D</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="486"/>
        <source>in basket</source>
        <translation>im Korb</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="487"/>
        <source>Ctrl+I</source>
        <translation>Strg-I</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="488"/>
        <source>someday maybe</source>
        <translation>vielleicht eines Tages</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="489"/>
        <source>Ctrl+S</source>
        <translation>Strg-S</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="490"/>
        <source>find</source>
        <translation>suche</translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="422"/>
        <source>Click here or press &lt;em&gt;Ctrl-M&lt;/em&gt; to select the view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="ui_etmView.py" line="444"/>
        <source>Click here or press &lt;em&gt;Ctrl-C&lt;/em&gt; to choose the active calendars.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>whichDialog</name>
    <message>
        <location filename="ui_etmWhich.py" line="46"/>
        <source>which</source>
        <translation>welches</translation>
    </message>
    <message>
        <location filename="ui_etmWhich.py" line="47"/>
        <source>You have selected instance ...</source>
        <translation>Du hast Instanz ... gewählt</translation>
    </message>
</context>
</TS>
