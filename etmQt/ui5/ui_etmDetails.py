# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './etmDetails.ui'
#
# Created: Sun Dec  8 18:13:57 2013
#      by: PyQt5 UI code generator 5.1.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_detailDialog(object):
    def setupUi(self, detailDialog):
        detailDialog.setObjectName("detailDialog")
        detailDialog.resize(525, 266)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(detailDialog.sizePolicy().hasHeightForWidth())
        detailDialog.setSizePolicy(sizePolicy)
        detailDialog.setMinimumSize(QtCore.QSize(460, 140))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/etm.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        detailDialog.setWindowIcon(icon)
        detailDialog.setToolTip("")
        detailDialog.setStyleSheet("")
        detailDialog.setModal(False)
        self.gridLayout = QtWidgets.QGridLayout(detailDialog)
        self.gridLayout.setContentsMargins(8, 6, 8, 6)
        self.gridLayout.setVerticalSpacing(6)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.closeButton = QtWidgets.QToolButton(detailDialog)
        self.closeButton.setStyleSheet("border: 0px black")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/exit.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.closeButton.setIcon(icon1)
        self.closeButton.setIconSize(QtCore.QSize(26, 26))
        self.closeButton.setObjectName("closeButton")
        self.horizontalLayout.addWidget(self.closeButton)
        self.line = QtWidgets.QFrame(detailDialog)
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.horizontalLayout.addWidget(self.line)
        self.fileButton = QtWidgets.QToolButton(detailDialog)
        self.fileButton.setStyleSheet("border: 0px black")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/fileopen.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.fileButton.setIcon(icon2)
        self.fileButton.setIconSize(QtCore.QSize(24, 24))
        self.fileButton.setObjectName("fileButton")
        self.horizontalLayout.addWidget(self.fileButton)
        self.itemButton = QtWidgets.QToolButton(detailDialog)
        self.itemButton.setStyleSheet("border: 0px black")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/edit_item.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.itemButton.setIcon(icon3)
        self.itemButton.setIconSize(QtCore.QSize(22, 22))
        self.itemButton.setObjectName("itemButton")
        self.horizontalLayout.addWidget(self.itemButton)
        self.cloneItemButton = QtWidgets.QToolButton(detailDialog)
        self.cloneItemButton.setStyleSheet("border: 0px")
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(":/copy.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.cloneItemButton.setIcon(icon4)
        self.cloneItemButton.setIconSize(QtCore.QSize(24, 24))
        self.cloneItemButton.setObjectName("cloneItemButton")
        self.horizontalLayout.addWidget(self.cloneItemButton)
        self.cloneActionButton = QtWidgets.QToolButton(detailDialog)
        self.cloneActionButton.setStyleSheet("border: 0px")
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap(":/action_start.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.cloneActionButton.setIcon(icon5)
        self.cloneActionButton.setIconSize(QtCore.QSize(28, 28))
        self.cloneActionButton.setObjectName("cloneActionButton")
        self.horizontalLayout.addWidget(self.cloneActionButton)
        self.line_2 = QtWidgets.QFrame(detailDialog)
        self.line_2.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.horizontalLayout.addWidget(self.line_2)
        self.finishButton = QtWidgets.QToolButton(detailDialog)
        self.finishButton.setStyleSheet("border: 0px black")
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap(":/finish_task.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.finishButton.setIcon(icon6)
        self.finishButton.setIconSize(QtCore.QSize(22, 22))
        self.finishButton.setObjectName("finishButton")
        self.horizontalLayout.addWidget(self.finishButton)
        self.minusButton = QtWidgets.QToolButton(detailDialog)
        self.minusButton.setStyleSheet("border: 0px black")
        icon7 = QtGui.QIcon()
        icon7.addPixmap(QtGui.QPixmap(":/date_delete.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.minusButton.setIcon(icon7)
        self.minusButton.setIconSize(QtCore.QSize(24, 24))
        self.minusButton.setObjectName("minusButton")
        self.horizontalLayout.addWidget(self.minusButton)
        self.moveButton = QtWidgets.QToolButton(detailDialog)
        self.moveButton.setStyleSheet("border: 0px black")
        icon8 = QtGui.QIcon()
        icon8.addPixmap(QtGui.QPixmap(":/move.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.moveButton.setIcon(icon8)
        self.moveButton.setIconSize(QtCore.QSize(40, 34))
        self.moveButton.setObjectName("moveButton")
        self.horizontalLayout.addWidget(self.moveButton)
        self.line_3 = QtWidgets.QFrame(detailDialog)
        self.line_3.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.horizontalLayout.addWidget(self.line_3)
        self.repsButton = QtWidgets.QToolButton(detailDialog)
        self.repsButton.setStyleSheet("border: 0px")
        icon9 = QtGui.QIcon()
        icon9.addPixmap(QtGui.QPixmap(":/repetitions.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.repsButton.setIcon(icon9)
        self.repsButton.setIconSize(QtCore.QSize(28, 30))
        self.repsButton.setObjectName("repsButton")
        self.horizontalLayout.addWidget(self.repsButton)
        self.historyButton = QtWidgets.QToolButton(detailDialog)
        self.historyButton.setStyleSheet("border: 0px black")
        icon10 = QtGui.QIcon()
        icon10.addPixmap(QtGui.QPixmap(":/history.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.historyButton.setIcon(icon10)
        self.historyButton.setIconSize(QtCore.QSize(26, 26))
        self.historyButton.setObjectName("historyButton")
        self.horizontalLayout.addWidget(self.historyButton)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.helpButton = QtWidgets.QToolButton(detailDialog)
        self.helpButton.setStyleSheet("border: 0px black")
        icon11 = QtGui.QIcon()
        icon11.addPixmap(QtGui.QPixmap(":/help.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.helpButton.setIcon(icon11)
        self.helpButton.setIconSize(QtCore.QSize(22, 22))
        self.helpButton.setObjectName("helpButton")
        self.horizontalLayout.addWidget(self.helpButton)
        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        self.detailFrame = QtWidgets.QFrame(detailDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.detailFrame.sizePolicy().hasHeightForWidth())
        self.detailFrame.setSizePolicy(sizePolicy)
        self.detailFrame.setMinimumSize(QtCore.QSize(0, 0))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.detailFrame.setFont(font)
        self.detailFrame.setFocusPolicy(QtCore.Qt.WheelFocus)
        self.detailFrame.setStyleSheet("")
        self.detailFrame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.detailFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.detailFrame.setObjectName("detailFrame")
        self.gridLayout.addWidget(self.detailFrame, 1, 0, 1, 1)
        self.statusBar = QtWidgets.QLineEdit(detailDialog)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.statusBar.setFont(font)
        self.statusBar.setFocusPolicy(QtCore.Qt.WheelFocus)
        self.statusBar.setAutoFillBackground(False)
        self.statusBar.setStyleSheet("background-color: transparent; border: 0px")
        self.statusBar.setReadOnly(True)
        self.statusBar.setObjectName("statusBar")
        self.gridLayout.addWidget(self.statusBar, 2, 0, 1, 1)

        self.retranslateUi(detailDialog)
        QtCore.QMetaObject.connectSlotsByName(detailDialog)

    def retranslateUi(self, detailDialog):
        _translate = QtCore.QCoreApplication.translate
        detailDialog.setWindowTitle(_translate("detailDialog", "details"))
        self.closeButton.setToolTip(_translate("detailDialog", "Click here or press <em>Ctrl-W</em> to close this dialog."))
        self.closeButton.setText(_translate("detailDialog", "Close"))
        self.fileButton.setToolTip(_translate("detailDialog", "Click here or press <em>Ctrl-E</em> to edit the file containing this item."))
        self.fileButton.setText(_translate("detailDialog", "File"))
        self.itemButton.setToolTip(_translate("detailDialog", "Click here or press <em>Return</em> to edit this item."))
        self.itemButton.setText(_translate("detailDialog", "Edit"))
        self.cloneItemButton.setToolTip(_translate("detailDialog", "Click here or press <em>Ctrl-C</em> to edit a copy of this item."))
        self.cloneItemButton.setText(_translate("detailDialog", "..."))
        self.cloneActionButton.setToolTip(_translate("detailDialog", "Click here or press <em>Ctrl-T</em> to start the timer for an action based on this item."))
        self.cloneActionButton.setText(_translate("detailDialog", "..."))
        self.finishButton.setToolTip(_translate("detailDialog", "Click here or press <em>Ctrl-F</em> when a task is selected to set the completion datetime for the task. "))
        self.finishButton.setText(_translate("detailDialog", "Finish"))
        self.minusButton.setToolTip(_translate("detailDialog", "Click here or press <em>Ctrl-X</em> to delete datetime(s) from this item."))
        self.minusButton.setText(_translate("detailDialog", "Delete"))
        self.moveButton.setToolTip(_translate("detailDialog", "Click here or press <em>Ctrl-M</em> to move this item to another file."))
        self.moveButton.setText(_translate("detailDialog", "..."))
        self.repsButton.setToolTip(_translate("detailDialog", "Click here or press <em>Ctrl-R</em> when a repeating item is selected to show the repetitions for the item."))
        self.repsButton.setText(_translate("detailDialog", "..."))
        self.historyButton.setToolTip(_translate("detailDialog", "Click here or press <em>Ctrl-H</em> to view the history of changes for the file containing this item."))
        self.historyButton.setText(_translate("detailDialog", "..."))
        self.helpButton.setToolTip(_translate("detailDialog", "Click here or press \'F1\' to view help information."))
        self.helpButton.setText(_translate("detailDialog", "..."))

