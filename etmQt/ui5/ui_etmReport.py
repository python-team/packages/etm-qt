# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './etmReport.ui'
#
# Created: Sun Dec  8 18:13:57 2013
#      by: PyQt5 UI code generator 5.1.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_reportDialog(object):
    def setupUi(self, reportDialog):
        reportDialog.setObjectName("reportDialog")
        reportDialog.resize(512, 264)
        reportDialog.setMinimumSize(QtCore.QSize(500, 0))
        reportDialog.setSizeIncrement(QtCore.QSize(21, 0))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/etm.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        reportDialog.setWindowIcon(icon)
        reportDialog.setStyleSheet("font-size: 10pt")
        self.gridLayout = QtWidgets.QGridLayout(reportDialog)
        self.gridLayout.setContentsMargins(8, 6, 8, 6)
        self.gridLayout.setVerticalSpacing(5)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.closeButton = QtWidgets.QToolButton(reportDialog)
        self.closeButton.setStyleSheet("border: 0px")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/exit.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.closeButton.setIcon(icon1)
        self.closeButton.setIconSize(QtCore.QSize(24, 24))
        self.closeButton.setObjectName("closeButton")
        self.horizontalLayout_2.addWidget(self.closeButton)
        self.line = QtWidgets.QFrame(reportDialog)
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.horizontalLayout_2.addWidget(self.line)
        self.printButton = QtWidgets.QToolButton(reportDialog)
        self.printButton.setStyleSheet("border: 0px")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/print.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.printButton.setIcon(icon2)
        self.printButton.setIconSize(QtCore.QSize(24, 24))
        self.printButton.setObjectName("printButton")
        self.horizontalLayout_2.addWidget(self.printButton)
        self.exportButton = QtWidgets.QToolButton(reportDialog)
        self.exportButton.setStyleSheet("border: 0px")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/export.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.exportButton.setIcon(icon3)
        self.exportButton.setIconSize(QtCore.QSize(22, 22))
        self.exportButton.setObjectName("exportButton")
        self.horizontalLayout_2.addWidget(self.exportButton)
        self.mailButton = QtWidgets.QToolButton(reportDialog)
        self.mailButton.setStyleSheet("border: 0pt")
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(":/send_email.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.mailButton.setIcon(icon4)
        self.mailButton.setIconSize(QtCore.QSize(26, 26))
        self.mailButton.setObjectName("mailButton")
        self.horizontalLayout_2.addWidget(self.mailButton)
        self.previous = QtWidgets.QToolButton(reportDialog)
        self.previous.setStyleSheet("border: 0px")
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap(":/previous.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.previous.setIcon(icon5)
        self.previous.setIconSize(QtCore.QSize(18, 18))
        self.previous.setObjectName("previous")
        self.horizontalLayout_2.addWidget(self.previous)
        self.search = QtWidgets.QLineEdit(reportDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.search.sizePolicy().hasHeightForWidth())
        self.search.setSizePolicy(sizePolicy)
        self.search.setMinimumSize(QtCore.QSize(100, 24))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.search.setFont(font)
        self.search.setObjectName("search")
        self.horizontalLayout_2.addWidget(self.search)
        self.next = QtWidgets.QToolButton(reportDialog)
        self.next.setStyleSheet("border: 0px")
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap(":/next.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.next.setIcon(icon6)
        self.next.setIconSize(QtCore.QSize(18, 18))
        self.next.setObjectName("next")
        self.horizontalLayout_2.addWidget(self.next)
        self.status_message = QtWidgets.QLineEdit(reportDialog)
        self.status_message.setMinimumSize(QtCore.QSize(100, 0))
        self.status_message.setFocusPolicy(QtCore.Qt.NoFocus)
        self.status_message.setStyleSheet("background: transparent; border: 0pt")
        self.status_message.setObjectName("status_message")
        self.horizontalLayout_2.addWidget(self.status_message)
        self.helpButton = QtWidgets.QToolButton(reportDialog)
        self.helpButton.setStyleSheet("border: 0px")
        icon7 = QtGui.QIcon()
        icon7.addPixmap(QtGui.QPixmap(":/help.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.helpButton.setIcon(icon7)
        self.helpButton.setIconSize(QtCore.QSize(20, 20))
        self.helpButton.setObjectName("helpButton")
        self.horizontalLayout_2.addWidget(self.helpButton)
        self.gridLayout.addLayout(self.horizontalLayout_2, 0, 0, 1, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.minusButton = QtWidgets.QToolButton(reportDialog)
        self.minusButton.setStyleSheet("border: 0px")
        icon8 = QtGui.QIcon()
        icon8.addPixmap(QtGui.QPixmap(":/remove_report.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.minusButton.setIcon(icon8)
        self.minusButton.setIconSize(QtCore.QSize(20, 20))
        self.minusButton.setObjectName("minusButton")
        self.horizontalLayout.addWidget(self.minusButton)
        self.saveButton = QtWidgets.QToolButton(reportDialog)
        self.saveButton.setStyleSheet("border: 0px")
        icon9 = QtGui.QIcon()
        icon9.addPixmap(QtGui.QPixmap(":/save.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.saveButton.setIcon(icon9)
        self.saveButton.setIconSize(QtCore.QSize(20, 20))
        self.saveButton.setObjectName("saveButton")
        self.horizontalLayout.addWidget(self.saveButton)
        self.reportBox = QtWidgets.QComboBox(reportDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.reportBox.sizePolicy().hasHeightForWidth())
        self.reportBox.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Courier New")
        font.setPointSize(14)
        self.reportBox.setFont(font)
        self.reportBox.setStyleSheet("QComboBox{font-size: 14pt; font-family: Courier New; background-color: white; selection-background-color:rgb(168, 207, 255); selection-color: black;}tooltip{font-family:Lucida Grande;font-size:11pt}\n"
"")
        self.reportBox.setEditable(True)
        self.reportBox.setInsertPolicy(QtWidgets.QComboBox.InsertAlphabetically)
        self.reportBox.setIconSize(QtCore.QSize(16, 7))
        self.reportBox.setFrame(True)
        self.reportBox.setObjectName("reportBox")
        self.horizontalLayout.addWidget(self.reportBox)
        self.reportButton = QtWidgets.QToolButton(reportDialog)
        self.reportButton.setStyleSheet("border: 0px")
        icon10 = QtGui.QIcon()
        icon10.addPixmap(QtGui.QPixmap(":/refresh.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.reportButton.setIcon(icon10)
        self.reportButton.setIconSize(QtCore.QSize(20, 20))
        self.reportButton.setObjectName("reportButton")
        self.horizontalLayout.addWidget(self.reportButton)
        self.gridLayout.addLayout(self.horizontalLayout, 2, 0, 1, 1)
        self.reportBrowser = QtWidgets.QTextBrowser(reportDialog)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.reportBrowser.setFont(font)
        self.reportBrowser.setStyleSheet("")
        self.reportBrowser.setLineWidth(1)
        self.reportBrowser.setMidLineWidth(0)
        self.reportBrowser.setReadOnly(True)
        self.reportBrowser.setObjectName("reportBrowser")
        self.gridLayout.addWidget(self.reportBrowser, 1, 0, 1, 1)

        self.retranslateUi(reportDialog)
        QtCore.QMetaObject.connectSlotsByName(reportDialog)

    def retranslateUi(self, reportDialog):
        _translate = QtCore.QCoreApplication.translate
        reportDialog.setWindowTitle(_translate("reportDialog", "report"))
        self.closeButton.setToolTip(_translate("reportDialog", "Click here or press Ctrl-W to close this view."))
        self.closeButton.setText(_translate("reportDialog", "..."))
        self.printButton.setToolTip(_translate("reportDialog", "Click here or press Ctrl-P to print this report."))
        self.printButton.setText(_translate("reportDialog", "..."))
        self.exportButton.setToolTip(_translate("reportDialog", "Click here or press Ctrl-E to export this report in CSV format."))
        self.exportButton.setText(_translate("reportDialog", "..."))
        self.mailButton.setToolTip(_translate("reportDialog", "Click here or press Ctrl-M to email this report."))
        self.mailButton.setText(_translate("reportDialog", "..."))
        self.previous.setToolTip(_translate("reportDialog", "Click here or press Shift-Ctrl-G to search backwards."))
        self.previous.setText(_translate("reportDialog", "..."))
        self.search.setToolTip(_translate("reportDialog", "Click here or press Ctrl-F to enter a search expression."))
        self.search.setPlaceholderText(_translate("reportDialog", "search"))
        self.next.setToolTip(_translate("reportDialog", "Click here or press Ctrl-G to search forwards."))
        self.next.setText(_translate("reportDialog", "..."))
        self.helpButton.setToolTip(_translate("reportDialog", "Click here or press \'F1\' to view help information."))
        self.helpButton.setText(_translate("reportDialog", "..."))
        self.minusButton.setToolTip(_translate("reportDialog", "Click here or press Delete to remove the selected report."))
        self.minusButton.setText(_translate("reportDialog", "..."))
        self.saveButton.setToolTip(_translate("reportDialog", "Click here or press Ctrl-S  to save the changes to the list of reports."))
        self.saveButton.setText(_translate("reportDialog", "..."))
        self.reportBox.setToolTip(_translate("reportDialog", "report specification"))
        self.reportButton.setToolTip(_translate("reportDialog", "Click here or press Ctrl-R to refresh the report\n"
"using the current specification."))
        self.reportButton.setText(_translate("reportDialog", "..."))
        self.reportBrowser.setToolTip(_translate("reportDialog", "report specificaton"))

